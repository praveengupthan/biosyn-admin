  <!-- sidebar menu area start -->
  <div class="sidebar-menu">
            <div class="sidebar-header">
                <div class="logo">
                    <a href="index.html"><img src="assets/images/icon/logo.png" alt="logo"></a>
                </div>
            </div>
            <div class="main-menu">
                <div class="menu-inner">
                    <nav>
                        <ul class="metismenu" id="menu">
                            <li class="active"><a href="dashboard.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
                            <li><a href="categories.php"><i class="fa fa-list"></i> <span>Categories</span></a></li>
                            <li><a href="products.php"><i class="fa fa-hourglass"></i> <span>Products</span></a></li>
                            <li><a href="enquiries.php"><i class="fa fa-headphones"></i> <span>Enquiries</span></a></li>
                            <li><a href="settings.php"><i class="fa fa-gear"></i> <span>Settings</span></a></li>
                            <li><a href="index.php"><i class="fa fa-sign-out"></i> <span>Logout</span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
        <!-- sidebar menu area end -->