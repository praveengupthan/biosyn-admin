<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Biosyn Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'styles.php' ?>
</head>

<body>
     <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->
    <!-- page container area start -->
    <div class="page-container">
      <?php include 'sidebar.php' ?>
        <!-- main content area start -->
        <div class="main-content">
           <?php include 'header.php' ?>
            <!-- page title area start -->
            <div class="page-title-area">
                <div class="row align-items-center py-3">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">New Product</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="index.php">Home</a></li>
                                <li><a href="products.php">Products</a></li>
                                <li><span>New Products</span></li>
                            </ul>
                        </div>
                    </div>   
                    <!-- col -->
                    <div class="col-lg-6 text-right">
                        <button onclick="window.location.href='products.php';" type="submit" class="btn btn-success mb-3" id="btnSaveProduct">Save Product</button>
                        <button onclick="window.location.href='products.php';" type="submit" class="btn btn-success mb-3">Cancel</button>
                    </div>
                    <!--/ col -->      
                    
                   
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">
                <div class="alert alert-success alert-dismissible" id="successAddProduct">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Success!</strong> This alert box could indicate a successful or positive action.
                </div>               
            <!-- row -->
            <div class="row mt-5">
                <!-- left col -->
                <div class="col-lg-8">
                    <!-- card -->
                    <div class="card">
                        <!-- card body -->
                        <div class="card-body">
                    <!-- row -->
                    <div class="row">
                        <!--col -->                      
                        <div class="col-lg-6">
                            <div class="form-group">
                                <label class="col-form-label">Select Category</label>
                                <select class="form-control">
                                    <option>Category</option>
                                    <option>Acetophenones</option>
                                    <option>benzophenones</option>
                                </select>
                            </div>
                        </div>
                        <!--/ col -->   
                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Product Name</label>
                                <input class="form-control" type="text" placeholder="Product Name" id="example-text-input">
                            </div>
                         </div>
                        <!--/ col -->  
                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Product Code</label>
                                <input class="form-control" type="text" placeholder="Product Code" id="example-text-input">
                            </div>
                         </div>
                        <!--/ col -->
                          <!-- col -->
                          <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">CAS Number</label>
                                <input class="form-control" type="text" placeholder="CAS Number" id="example-text-input">
                            </div>
                         </div>
                        <!--/ col -->
                        
                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">MDL No</label>
                                <input class="form-control" type="text" placeholder="MDL No" id="example-text-input">
                            </div>
                         </div>
                        <!--/ col -->
                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Chemical Formula</label>
                                <input class="form-control" type="text" placeholder="Chemical Formula" id="example-text-input">
                            </div>
                         </div>
                        <!--/ col -->
                          <!-- col -->
                          <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Molecular Weight</label>
                                <input class="form-control" type="text" placeholder="Molecular Weight" id="example-text-input">
                            </div>
                         </div>
                        <!--/ col -->
                         <!-- col -->
                         <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Appearance</label>
                                <input class="form-control" type="text" placeholder="Appearance" id="example-text-input">
                            </div>
                         </div>
                        <!--/ col -->
                           <!-- col -->
                           <div class="col-lg-6">
                            <div class="form-group">
                                <label for="example-text-input" class="col-form-label">Purity (GC)</label>
                                <input class="form-control" type="text" placeholder="Purity (GC)" id="example-text-input">
                            </div>
                         </div>
                        <!--/ col -->                  
                    </div>
                    <!--/ row -->
                    </div>
                    <!--/ card body -->
                    </div>
                    <!--/ card -->
                </div>
                <!--/ left col -->

                <!-- right col -->
                <div class="col-lg-4">
                    <!-- card -->
                    <div class="card">
                        <!-- card body -->
                        <div class="card-body">
                            <!-- row -->
                            <div class="row">
                                <!-- col -->
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="example-text-input" class="col-form-label">Product Describe</label>
                                        <textarea class="form-control" style="height:150px;" placeholder="Start Product Describe"></textarea>
                                    </div>
                                </div>
                                <!--/ col --> 
                                <!-- col -->
                                <div class="col-lg-12">
                                    <div class="input-group">
                                        <div class="custom-file">
                                            <input type="file" class="custom-file-input" id="inputGroupFile04">
                                            <label class="custom-file-label" for="inputGroupFile04">Choose file</label>
                                        </div>
                                        <div class="input-group-append">
                                            <button class="btn btn-outline-secondary" type="button">Upload</button>
                                        </div>
                                    </div>
                                </div>
                                <!--/ col -->
                                 <!-- col -->
                                 <div class="col-lg-12 mt-3">
                                    <div class="uploadimg">
                                        <button type="button" class="btn btn-flat btn-danger btn-xs mb-3 position-absolute"><i class="fa fa-trash"></i></button>
                                        <img src="assets/images/big-product.png" alt="" class="img-fluid">
                                    </div>
                                </div>
                                <!--/ col -->    
                            </div>
                            <!--/ row -->
                        </div>
                        <!--/ card body -->
                    </div>
                    <!--/ card -->
                </div>
                <!--/ right col -->
            </div>
            <!--/ row -->
            
            </div>
            <!-- main content area end -->
            <?php include 'footer.php' ?>
    </div>
    <!-- page container area end -->    
    

    <?php include 'scripts.php' ?>

    <script>
        $(document).ready(function(){
            $("#successAddProduct").hide();

            $("#btnSaveProduct").click(function(){
                $("#successAddProduct").show();
            });
        });
    </script>
</body>

</html>