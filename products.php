<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Biosyn Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'styles.php' ?>
</head>

<body>
     <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->
    <!-- page container area start -->
    <div class="page-container">
      <?php include 'sidebar.php' ?>
        <!-- main content area start -->
        <div class="main-content">
           <?php include 'header.php' ?>
            <!-- page title area start -->
            <div class="page-title-area">
                <div class="row align-items-center py-3">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Products</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="index.html">Home</a></li>
                                <li><span>Products</span></li>
                            </ul>
                        </div>
                    </div>                    
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">

            <!-- table data -->

            <div class="card">
                <div class="card-body">
                    <div class="d-flex justify-content-between">
                        <h4 class="header-title">List of Products</h4>
                        <button onclick="window.location.href='product-new.php';" type="submit" class="btn btn-primary mb-3"><i class="fa fa-plus-square"></i> New Product</button>
                   </div>
                    <div class="data-tables">
                        <table id="dataTable" class="text-center">
                            <thead class="bg-light text-capitalize">
                                <tr>
                                    <th>Category</th>
                                    <th>Product Name</th>
                                    <th>CAS No</th>
                                    <th>Product Code</th>
                                    <th>MDL No</th>
                                    <th>Edit</th>
                                    <th>Show in Home</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Acetophenones</td>
                                    <td><a href="javascript:void(0)">4’-Acetoxy-2’-hydroxy acetophenone</a></td>
                                    <td>98-86-2</td>
                                    <td>FA03663</td>
                                    <td>MFCD00008724</td>
                                    <td><a href="product-new.php"><i class="fa fa-edit"></i></a></td>
                                    <td><input type="checkbox"></td>
                                    <td><a href="javascript:void(0)"><i class="fa fa-trash"></i></a></td>
                                </tr>

                                <tr>
                                    <td>Benzophenones</td>
                                    <td><a href="javascript:void(0)">5’-Acetoxy-2’-hydroxy acetophenone</a></td>
                                    <td>98-86-2</td>
                                    <td>FA03663</td>
                                    <td>MFCD00008724</td>
                                    <td><a href="product-new.php"><i class="fa fa-edit"></i></a></td>
                                    <td><input type="checkbox"></td>
                                    <td><a href="javascript:void(0)"><i class="fa fa-trash"></i></a></td>
                                </tr>

                                <tr>
                                    <td>Carbohydrates</td>
                                    <td><a href="javascript:void(0)">3`-Benzyloxy-2`,4`-dimethoxy-6`-hydroxy acetophenone</a></td>
                                    <td>98-86-2</td>
                                    <td>B1-103</td>
                                    <td>MFCD00008724</td>
                                    <td><a href="product-new.php"><i class="fa fa-edit"></i></a></td>
                                    <td><input type="checkbox"></td>
                                    <td><a href="javascript:void(0)"><i class="fa fa-trash"></i></a></td>
                                </tr>

                                <tr>
                                    <td>Coumarins</td>
                                    <td><a href="javascript:void(0)">4’-Benzyloxy-2’-methoxyacetophenone</a></td>
                                    <td>98-86-2</td>
                                    <td>B1-104</td>
                                    <td>MFCD00008724</td>
                                    <td><a href="product-new.php"><i class="fa fa-edit"></i></a></td>
                                    <td><input type="checkbox"></td>
                                    <td><a href="javascript:void(0)"><i class="fa fa-trash"></i></a></td>
                                </tr>

                                <tr>
                                    <td>Chalcones</td>
                                    <td><a href="product-new.php">4’-Benzyloxy acetophenone</a></td>
                                    <td>98-86-2</td>
                                    <td>B1-105</td>
                                    <td>MFCD00008724</td>
                                    <td><a href="javascript:void(0)"><i class="fa fa-edit"></i></a></td>
                                    <td><input type="checkbox"></td>
                                    <td><a href="javascript:void(0)"><i class="fa fa-trash"></i></a></td>
                                </tr>

                                <tr>
                                    <td>Chromones & Chromanones</td>
                                    <td><a href="javascript:void(0)">2`-Benzoyloxy-4`-methoxy acetophenone</a></td>
                                    <td>98-86-2</td>
                                    <td>B1-106</td>
                                    <td>MFCD00008724</td>
                                    <td><a href="javascript:void(0)"><i class="fa fa-edit"></i></a></td>
                                    <td><input type="checkbox"></td>
                                    <td><a href="javascript:void(0)"><i class="fa fa-trash"></i></a></td>
                                </tr>

                                <tr>
                                    <td>Flavones & Flavanones</td>
                                    <td><a href="javascript:void(0)">3’-Bromo-5’-chloro-2’-hydroxyacetophenone</a></td>
                                    <td>98-86-2</td>
                                    <td>B1-107</td>
                                    <td>MFCD00008724</td>
                                    <td><a href="javascript:void(0)"><i class="fa fa-edit"></i></a></td>
                                    <td><input type="checkbox"></td>
                                    <td><a href="javascript:void(0)"><i class="fa fa-trash"></i></a></td>
                                </tr>

                                <tr>
                                    <td>Natural Products</td>
                                    <td><a href="javascript:void(0)">	5’-Acetoxy-2’-hydroxy acetophenone</a></td>
                                    <td>98-86-2</td>
                                    <td>B1-102</td>
                                    <td>MFCD00008724</td>
                                    <td><a href="javascript:void(0)"><i class="fa fa-edit"></i></a></td>
                                    <td><input type="checkbox"></td>
                                    <td><a href="javascript:void(0)"><i class="fa fa-trash"></i></a></td>
                                </tr>
                              
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>          

            <!--/ table data -->
            </div>
            <!-- main content area end -->
            <?php include 'footer.php' ?>
    </div>
    <!-- page container area end -->

    <?php include 'scripts.php' ?>
</body>

</html>