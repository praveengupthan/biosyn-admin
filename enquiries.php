<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Biosyn Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'styles.php' ?>
</head>

<body>
     <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->
    <!-- page container area start -->
    <div class="page-container">
      <?php include 'sidebar.php' ?>
        <!-- main content area start -->
        <div class="main-content">
           <?php include 'header.php' ?>
            <!-- page title area start -->
            <div class="page-title-area">
                <div class="row align-items-center py-3">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Enquiries</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="index.html">Home</a></li>
                                <li><span>Enquiries</span></li>
                            </ul>
                        </div>
                    </div>                    
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">

            <!-- table card -->
            <div class="card">
                <div class="card-body">
                   <div class="d-flex justify-content-between">
                        <h4 class="header-title">List of Enquiries</h4>
                        <!-- <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#NewCategory"><i class="fa fa-plus-square"></i> New Category</button> -->
                   </div>
                    <div class="single-table">
                        <div class="table-responsive">
                            <table class="table table-bordered text-center">
                                <thead class="text-uppercase">
                                    <tr>
                                        <th scope="col">S.No:</th>
                                        <th scope="col">Name</th>
                                        <th scope="col">Company Name</th>  
                                        <th scope="col">Location</th>  
                                        <th scope="col">Date of Enquiry</th>                                    
                                        <th scope="col">Phone</th>
                                        <th scope="col">Delete</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td><a data-toggle="modal" data-target="#enquiriesView" href="javascript:void(0)">Praveen Guptha</a></td>   
                                        <td>Core Web Pro</td>                                      
                                        <td>Hyderabad, Telangana</td> 
                                        <td>09-05-2020</td> 
                                        <td>+91 9642123254</td>                                        
                                        <td><a href="javascript:void(0)"><i class="fa fa-trash"></i></a></td>
                                    </tr>   
                                    <tr>
                                        <th scope="row">2</th>
                                        <td><a href="javascript:void(0)">Praveen</a> </td>   
                                        <td>Core Web Pro</td>                                      
                                        <td>Hyderabad, Telangana</td> 
                                        <td>09-05-2020</td> 
                                        <td>+91 9642123254</td>                                        
                                        <td><a href="javascript:void(0)"><i class="fa fa-trash"></i></a></td>
                                    </tr> 
                                    <tr>
                                        <th scope="row">3</th>
                                        <td> <a href="javascript:void(0)">Guptha</a></td>   
                                        <td>Core Web Pro</td>                                      
                                        <td>Hyderabad, Telangana</td> 
                                        <td>09-05-2020</td> 
                                        <td>+91 9642123254</td>                                        
                                        <td><a href="javascript:void(0)"><i class="fa fa-trash"></i></a></td>
                                    </tr> 
                                    <tr>
                                        <th scope="row">4</th>
                                        <td><a href="javascript:void(0)">Nandipati</a></td>   
                                        <td>Core Web Pro</td>                                      
                                        <td>Hyderabad, Telangana</td> 
                                        <td>09-05-2020</td> 
                                        <td>+91 9642123254</td>                                        
                                        <td><a href="javascript:void(0)"><i class="fa fa-trash"></i></a></td>
                                    </tr> 
                                    <tr>
                                        <th scope="row">5</th>
                                        <td><a href="javascript:void(0)">Radha Krishna</a></td>   
                                        <td>Core Web Pro</td>                                      
                                        <td>Hyderabad, Telangana</td> 
                                        <td>09-05-2020</td> 
                                        <td>+91 9642123254</td>                                        
                                        <td><a href="javascript:void(0)"><i class="fa fa-trash"></i></a></td>
                                    </tr>    
                                           
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!--/ table -->
            </div>
            <!-- main content area end -->
            <?php include 'footer.php' ?>
    </div>
    <!-- page container area end -->

     <!-- Modal -->
     <div class="modal fade" id="enquiriesView">
         <!-- new category modal -->
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Praveen Guptha Nandipati</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                    <!-- details of enquiry -->
                    <div class="enquiry-view">
                        <dl>
                            <dt>Company Name:</dt>
                            <dd>Core Web Pro</dd>
                        </dl>
                        <dl>
                            <dt>Designation:</dt>
                            <dd>Asst. Manager Operations</dd>
                        </dl>
                        <dl>
                            <dt>Address:</dt>
                            <dd>91, Allwyn Colony Phase - I, Kamala Prasanna Nagar, Kukatpally, Hyderabad</dd>
                        </dl>
                        <dl>
                            <dt>State:</dt>
                            <dd>Telangana</dd>
                        </dl>
                        <dl>
                            <dt>Phone Number:</dt>
                            <dd>+ 91 9642123254</dd>
                        </dl>
                        <dl>
                            <dt>Email:</dt>
                            <dd>praveennandipati@gmail.com</dd>
                        </dl>
                        <dl>
                            <dt>Date of Message:</dt>
                            <dd>05-05-2020, 20.30 hrs</dd>
                        </dl>
                        <dl>
                            <dt>Enquiry Message:</dt>
                            <dd>Lorem ipsum dolor sit amet consectetur adipisicing elit. Quo corrupti atque incidunt laborum nulla voluptatem perferendis magnam porro quis. Aliquam itaque consequatur consequuntur voluptates quia.</dd>
                        </dl>
                    </div>
                    <!--/ detail of enquiry -->
                </div>                
            </div>
        </div>
    </div>
    <!--/ new category modal -->
    

    <?php include 'scripts.php' ?>
</body>

</html>