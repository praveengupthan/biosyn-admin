<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Biosyn Admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include 'styles.php' ?>
</head>

<body>
     <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
    <!-- preloader area start -->
    <div id="preloader">
        <div class="loader"></div>
    </div>
    <!-- preloader area end -->
    <!-- page container area start -->
    <div class="page-container">
      <?php include 'sidebar.php' ?>
        <!-- main content area start -->
        <div class="main-content">
           <?php include 'header.php' ?>
            <!-- page title area start -->
            <div class="page-title-area">
                <div class="row align-items-center py-3">
                    <div class="col-sm-6">
                        <div class="breadcrumbs-area clearfix">
                            <h4 class="page-title pull-left">Categories</h4>
                            <ul class="breadcrumbs pull-left">
                                <li><a href="index.html">Home</a></li>
                                <li><span>Categories</span></li>
                            </ul>
                        </div>
                    </div>                    
                </div>
            </div>
            <!-- page title area end -->
            <div class="main-content-inner">

            <!-- table card -->
            <div class="card">
                <div class="card-body">
                   <div class="d-flex justify-content-between">
                        <h4 class="header-title">List of Categories</h4>
                        <button type="button" class="btn btn-primary mb-3" data-toggle="modal" data-target="#NewCategory"><i class="fa fa-plus-square"></i> New Category</button>
                   </div>
                    <div class="single-table">
                        <div class="table-responsive">
                            <table class="table table-bordered text-center">
                                <thead class="text-uppercase">
                                    <tr>
                                        <th scope="col">S.No:</th>
                                        <th scope="col">Category Name</th>                                       
                                        <th scope="col">Edit</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th scope="row">1</th>
                                        <td>Acetophenones</td>                                        
                                        <td><a href="javascript:void(0)"><i class="fa fa-edit"></i></a></td>
                                       
                                    </tr>      
                                    <tr>
                                        <th scope="row">2</th>
                                        <td>benzophenones</td>                                        
                                        <td><a href="javascript:void(0)"><i class="fa fa-edit"></i></a></td>
                                       
                                    </tr>    
                                    <tr>
                                        <th scope="row">3</th>
                                        <td>Carbohydrates</td>                                        
                                        <td><a href="javascript:void(0)"><i class="fa fa-edit"></i></a></td>
                                        
                                    </tr>      
                                    <tr>
                                        <th scope="row">4</th>
                                        <td>Coumarins</td>                                        
                                        <td><a href="javascript:void(0)"><i class="fa fa-edit"></i></a></td>
                                        
                                    </tr>     
                                    <tr>
                                        <th scope="row">5</th>
                                        <td>Chalcones</td>                                        
                                        <td><a href="javascript:void(0)"><i class="fa fa-edit"></i></a></td>
                                       
                                    </tr>       
                                    <tr>
                                        <th scope="row">6</th>
                                        <td>Chromones & Chromanones</td>                                        
                                        <td><a href="javascript:void(0)"><i class="fa fa-edit"></i></a></td>
                                       
                                    </tr>  
                                    <tr>
                                        <th scope="row">7</th>
                                        <td>Flavones & Flavanones</td>                                        
                                        <td><a href="javascript:void(0)"><i class="fa fa-edit"></i></a></td>
                                       
                                    </tr>     
                                    <tr>
                                        <th scope="row">8</th>
                                        <td>Natural Products</td>                                        
                                        <td><a href="javascript:void(0)"><i class="fa fa-edit"></i></a></td>
                                        
                                    </tr>   
                                    <tr>
                                        <th scope="row">9</th>
                                        <td>Fine Chemicals & API Intermediates</td>                                        
                                        <td><a href="javascript:void(0)"><i class="fa fa-edit"></i></a></td>
                                       
                                    </tr>  
                                    <tr>
                                        <th scope="row">10</th>
                                        <td>Other Range of Products</td>                                        
                                        <td><a href="javascript:void(0)"><i class="fa fa-edit"></i></a></td>
                                       
                                    </tr>  
                                    <tr>
                                        <th scope="row">11</th>
                                        <td>Isocyanates & Polyurethanes</td>                                        
                                        <td><a href="javascript:void(0)"><i class="fa fa-edit"></i></a></td>
                                       
                                    </tr>          
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!--/ table -->
            </div>
            <!-- main content area end -->
            <?php include 'footer.php' ?>
    </div>
    <!-- page container area end -->

     <!-- Modal -->
     <div class="modal fade" id="NewCategory">
         <!-- new category modal -->
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">New Category</h5>
                    <button type="button" class="close" data-dismiss="modal"><span>&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="example-text-input" class="col-form-label">Category Name</label>
                        <input class="form-control" type="text" value="Category Name" id="example-text-input">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
    <!--/ new category modal -->
    

    <?php include 'scripts.php' ?>
</body>

</html>